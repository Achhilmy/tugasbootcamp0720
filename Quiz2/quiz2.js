/**
 * IndonesiaMengoding
 * InfoQuiz
 * Silahkan buat folder baru seperti pada pengerjaan tugas dengan nama Quiz 2
 * Masukkan file quiz2.js ke dalam folder tersebut
 * Setelah selesai mengerjakan, push pekerjaan Anda ke repository gitlab
 * Input link commit hasil pekerjaan Anda ke sanbercode.com
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Kerjakan soal semampunya dengan baik dan jujur
 * Tidak perlu bertanya kepada trainer terkait soal, cukup dikerjakan sesuai pemahaman peserta
 * Peserta diperbolehkan untuk googling, namun tidak diperbolehkan untuk bertanya, berdiskusi, atau mencontek.
 * Selama quiz berlangsung, grup diskusi telegram akan dinonaktifkan (peserta tidak dapat mengirimkan pesan atau berdiskusi di grup)
 * 
 * Selamat mengerjakan
*/

/* 
  SOAL CLASS SCORE (15 poin+ 5 es6)
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number atau array of number.
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    constructor(subject, email, points) {
      this.subject = subject;
      this.email = email;
      this.points = points;
    }
  
    average() {
      let totalPoint = 0;
      for (let i = 0; i < this.points.length; i++) {
        totalPoint += this.points[i]
      }
      // return parseFloat(totalPoint / this.points.length)
      return (totalPoint / this.points.length).toFixed(1)
    }
  }
  
  /* 
    SOAL View Score (15 Poin + 5 Poin ES6)
    Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
    Function viewScores mengolah data email dan nilai skor pada parameter array 
    lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
    Contoh: 
    Input 
  
    data : 
    [
      ["email", "quiz-1", "quiz-2", "quiz-3"],
      ["abduh@mail.com", 78, 89, 90],
      ["khairun@mail.com", 95, 85, 88]
    ]
  
    subject: "quiz-1"
  
    Output 
    [
      {email: "abduh@mail.com", subject: "quiz-1", points: 78},
      {email: "khairun@mail.com", subject: "quiz-1", points: 95},
    ]
  */
  
  const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
  function viewScores(data, subject) {
    // code kamu di sini
    let scoreArr = []
    let subjectIndex;
    switch (subject) {
      case "quiz-1":
        subjectIndex = 1
        break;
      case "quiz-2":
        subjectIndex = 2
        break;
      case "quiz-3":
        subjectIndex = 3
        break;
      default:
        break;
    }
    for (let i = 1; i < data.length; i++) {
      const score = new Score(subject, data[i][0], data[i][subjectIndex])
      scoreArr.push(score)
    }
    console.log(scoreArr)
  }
  
  //TEST CASE
  viewScores(data, "quiz-1")
  viewScores(data, "quiz-2")
  viewScores(data, "quiz-3")
  
  /* 
    SOAL Recap Score (20 Poin + 5 Poin ES6)
      buatlah function recapScore menampilkan perolehan nilai semua student. 
      Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
      Predikat kelulusan ditentukan dari aturan berikut:
      nilai > 70 "participant"
      nilai > 80 "graduate"
      nilai > 90 "honour"
  
      contoh output:
      1. Email: abduh@mail.com
      Rata-rata: 85.7
      Predikat: graduate
  
      2. Email: khairun@mail.com
      Rata-rata: 89.3
      Predikat: graduate
  
      3. Email: bondra@mail.com
      Rata-rata: 74.3
      Predikat: participant
  
      4. Email: regi@mail.com
      Rata-rata: 91
      Predikat: honour
  
  */
  
  function recapScores(data) {
    // code kamu di sini
    let recapArr = []
    for (let i = 1; i < data.length; i++) {
      const [email, ...nilai] = data[i]
      const score = new Score('Recap', data[i][0], nilai)
      const rata2 = score.average()
  
      let predikat;
      if (rata2 > 90) {
        predikat = 'honour'
      } else if (rata2 > 80) {
        predikat = 'graduate'
      } else if (rata2 > 70) {
        predikat = 'participant'
      }
  
      // const predikat = rata2 > 90 ? 'honour' : rata2 > 80 ? 'graduate' : rata2 > 70 ? 'participant' : "don't give up!"
  
      const template =
        `   ${i}. Email: ${score.email}
     Rata-rata: ${rata2}
     Predikat: ${predikat}
     `
  
      console.log(template)
    }
  }
  
  // DRIVEN CODE
  recapScores(data)