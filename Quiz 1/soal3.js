// Bandingkan Angka (10 poin)

// Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
// dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
// maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
// dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
// maka function akan mereturn nilai -1. 

function bandingkan(num1, num2 = 0) {
  // code di sini
  if (num1 < 0 || num2 < 0) {
    return -1
  } else if (num1 == num2) {
    return -1
  } else if (num1 > num2) {
    return num1
  } else if (num1 < num2) {
    return num2
  } else if (!num1) {
    return -1
  }
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
