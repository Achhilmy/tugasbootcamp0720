// Conditional Ascending Descending (15)

// Tulislah sebuah function ConditionalAscDesc yang menerima dua buah parameter dengan tipe Number. 
// Parameter number pertama diberi nama reference, dan parameter number kedua diberi nama check. 
// Function ini mirip seperti kedua function sebelumnya yaitu AscendingTen 
// dan DescendingTen yaitu akan menampilkan 10 angka berderet dimulai atau diakhiri dari reference. 
// Function ConditionalAscDesc mengecek jika parameter check merupakan ganjil 
// maka output yang ditampilkan yaitu deretan angka ascending, 
// jika parameter check merupakan angka genap maka output yang ditampilkan yaitu deretan angka descending. 
// Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan atau hanya satu saja parameter yang diberikan.

// Contoh: 
// console.log(ConditionalAscDesc(1, 1)) akan menampilkan 1 2 3 4 5 6 7 8 9 10
// console.log(ConditionalAscDesc(100, 4)) akan menampilkan 100 99 98 97 96 95 94 93 92 91

function ConditionalAscDesc(reference, check) {
  // Tulis code kamu di sini
  if (!reference || !check) {
    return -1
  } else if (check % 2 == 0) {
    var number = reference.toString();
    for (var i = 1; i <= 9; i++) {
      var add = reference - i;
      number = number + ' ' + add.toString()
    }
    return number
  } else if (check % 2 != 0) {
    var number = reference.toString();
    for (var i = 1; i <= 9; i++) {
      var add = reference + i;
      number = number + ' ' + add.toString()
    }
    return number
  }
}

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1
